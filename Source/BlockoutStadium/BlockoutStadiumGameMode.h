// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BlockoutStadiumGameMode.generated.h"

UCLASS(minimalapi)
class ABlockoutStadiumGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABlockoutStadiumGameMode();
};



