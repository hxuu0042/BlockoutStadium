// Copyright Epic Games, Inc. All Rights Reserved.

#include "BlockoutStadiumGameMode.h"
#include "BlockoutStadiumCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABlockoutStadiumGameMode::ABlockoutStadiumGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
